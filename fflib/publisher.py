__author__ = 'Faddey'


class Publisher(object):
    """
    Base class for implementing Publish/Subscribe pattern.
    """

    __subscribers = None

    def __init__(self):
        self.__subscribers = {}

    def subscribe(self, event, subscriber):
        """
        `subscriber` must be callable
        """
        if event not in self.__subscribers:
            self.__subscribers[event] = []
        self.__subscribers[event].append(subscriber)

    def publish(self, event, *args, **kwargs):
        if event in self.__subscribers:
            for subscriber in self.__subscribers[event]:
                subscriber(*args, **kwargs)

    def unsubscribe(self, event, subscriber):
        if event in self.__subscribers:
            if subscriber in self.__subscribers[event]:
                self.__subscribers[event].remove(subscriber)

    def unsubscribe_all(self, event):
        self.__subscribers[event] = []