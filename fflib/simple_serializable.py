import enum
import inspect
from . import utils as _


class SimpleSerializable:
    """
    Base class for creating objects that can be simply
    serialized to/deserialized from JSON.

    Features:
    1) serialization to JSON-friendly structure
    2) deserialization w/o knowing the exact class
    3) nice repr

    Limitations:
    1) constructor should be able to take all parameters in **kwargs form
    2) top-level structure of serialized format
       cannot be changed w/o breaking feature #2
    3) constructor arguments must be JSON-friendly; otherwise you may have
       to write dumping/parsing routines via protected methods.
    4) deserialization of dump retrieved from untrusted source is unsafe

    Public API:
    1) patch(**kwargs)
        creates new object with patched parameters
    2) dump()
        creates a structure that can be directly passed to json.dump.
    3) load(dump)  # staticmethod
        takes the structure returned from dump() and reconstructs the object

    Internal API:
    1) _get_dumped_params()
        returns dictionary that can be passed to the constructor as **kwargs
    2) _params_to_json(params)
        takes output of _get_dumped_params(), returns JSON-serializable structure
    3) _json_to_params(structure)
        reverse operation for _params_to_json():
        takes JSON-serializable structure, return output of _get_dumped_params()
    4) _format_argument, _format_argument_value
        overridable helpers for formatting repr
    """

    def patch(self, **args_patch):
        args = self._get_dumped_params()
        args.update(args_patch)
        cls = self.__class__
        return cls(**args)

    def dump(self):
        return {
            'type': _.get_import_path(self.__class__),
            'attrs': self._params_to_json(self._get_dumped_params())
        }

    @staticmethod
    def Load(dump):
        cls = _.load_by_import_path(dump['type'])
        return cls(**cls._json_to_params(dump['attrs']))

    def __repr__(self):
        attrs = self._get_dumped_params()
        attr_str_list = [
            self._format_argument(name, attrs[name])
            for name, has_default, default in self._get_init_params_info()
            if not has_default or attrs[name] != default
        ]
        return '{}({})'.format(
            self.__class__.__name__, ', '.join(attr_str_list)
        )

    def __eq__(self, other):
        if not isinstance(other, SimpleSerializable):
            raise TypeError("{0} can be compared only with other SimpleSerializable"
                            .format(self.__class__.__name__))
        return self.__class__ == other.__class__ \
            and self._get_dumped_params() == other._get_dumped_params()

    def _get_dumped_params(self):
        """
        :return: dictionary that can be passed to constructor as **kwargs
        """
        return dict(self.__dict__)

    def _params_to_json(self, params):
        """
        :param params: dictionary retrieved from _get_dumped_params()
        :return: JSON-serializable dictionary
        """
        return params

    @classmethod
    def _json_to_params(cls, params):
        """
        :param params: dictionary of parameters just deserialized from JSON
        :return: dictionary of parameters ready to be passed into constructor
        """
        return params

    def _format_argument(self, name, value):
        """Used in repr - converts a pair on param name and value into string"""
        return '{}={}'.format(name, self._format_argument_value(name, value))

    def _format_argument_value(self, name, value):
        """Used in repr - converts a value of parameter into string"""
        if isinstance(value, enum.Enum):
            value = value.value
        return repr(value)

    def _get_init_params_info(self):
        """
        Helper used to skip default parameters in repr
        :return list of tuples (param_name, value_is_default, default_value)
        """
        return [
            (name, param.default is not param.empty, param.default)
            for name, param in inspect.signature(self.__init__).parameters.items()
        ]
