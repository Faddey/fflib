import os
import ast
from collections import OrderedDict

from .data.tree import TreeIterator
from .parsing import TypeBasedRouter, route


def ast_by_path(path):
    with open(path) as f:
        return ast.parse(f.read(), os.path.split(path)[1])


def ast_to_dict(node):
    ret = OrderedDict()
    ret['__type__'] = type(node).__name__
    for field in node._fields:
        ret[field] = getattr(node, field)
    return ret


class CodeIterator(TreeIterator):

    default_leafs = (
        ast.Import, ast.ImportFrom, ast.Assign, ast.Expr, ast.Raise, ast.Return,
        ast.AugAssign
    )

    def __init__(self, root_ast, leafs=None, yield_inner=True):
        super(CodeIterator, self).__init__(root_ast, yield_inner)
        self._router = TypeBasedRouter(self)
        if leafs is None:
            leafs = self.default_leafs
        self._leaf_node_types = tuple(leafs)

    def get_value(self, node):
        return node

    def get_children(self, node):
        return self._router.select_route(node)(node)

    def is_leaf(self, node):
        return isinstance(node, self._leaf_node_types)

    @route(ast.Module, ast.ClassDef, ast.FunctionDef)
    def _children_from_body(self, node):
        return node.body

    @route(ast.For, ast.While, ast.If)
    def _children_from_flow_control(self, node):
        return node.body + node.orelse

    @route(ast.Import, ast.ImportFrom, ast.Assign, ast.Expr, ast.Raise,
           ast.Return, ast.AugAssign)
    def _no_children(self, node):
        return []

