import six
import operator
import collections
import functools
from .common import empty

__all__ = ['pipe', 'mapper', 'fmap', 'delegate_method', 'getter', 'attrgetter', 'itemgetter']


def pipe(first_function, *functions):
    if isinstance(first_function, collections.Iterable) and not functions:
        if not isinstance(first_function, (list, tuple)):
            first_function = list(first_function)
        functions = first_function[1:]
        first_function = first_function[0]

    def Pipe(value=empty):
        if value is empty:
            value = first_function()
        else:
            value = first_function(value)
        for func in functions:
            value = func(value)
        return value

    return Pipe


def mapper(function):
    return functools.partial(map, function)


if six.PY3:
    def fmap(function, *iterables):
        # stands from "forced map"
        return list(map(function, *iterables))
else:
    fmap = map


def delegate_method(method_name):
    def delegate(self, *args, **kwargs):
        return getattr(self, method_name)(*args, **kwargs)
    delegate.__name__ = method_name
    return delegate


def getter(item):
    if isinstance(item, (int, slice, tuple)):
        return operator.itemgetter(item)
    elif isinstance(item, str):
        _attr_getter = operator.attrgetter(item)
        _item_getter = operator.itemgetter(item)

        def Getter(obj):
            try:
                return _attr_getter(obj)
            except AttributeError:
                return _item_getter(obj)
        return Getter
    else:
        raise TypeError("Unknown kind of index: {}".format(repr(item)))


attrgetter = operator.attrgetter
itemgetter = operator.itemgetter

