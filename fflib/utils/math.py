import math

__all__ = ['biggest_divisor', 'lowest_divisor']


def biggest_divisor(n, limit=None):
    n = int(n)
    if limit is not None:
        limit = n / limit
    return n // lowest_divisor(n, limit)


def lowest_divisor(n, limit=None):
    n = int(n)
    if limit is None:
        limit = 2
        ret = 1
    else:
        limit = int(math.ceil(limit))
        ret = n
    for i in range(limit, (n // 2) + 1):
        if n % i == 0:
            ret = i
            break
    return ret

