from .common import is_collection


__all__ = ['iflatten', 'flatten', 'iflatten_values', 'flatten_values']


def iflatten(*args):
    """
    Arguments may be nested collections - then they will be flattened.
    Also arguments may be not iterable at all - then they simply yielded.
    Examples:
        flatten([1, [2, 3, [4]], 5]) -> [1, 2, 3, 4, 5]
        flatten([1], 2, 3, [[4, 5]]) -> [1, 2, 3, 4, 5]
    :param args: objects, iterable or not
    :return: generator that yields non-iterable objects
    """
    for arg in args:
        if is_collection(arg):
            for item in iflatten(*arg):
                yield item
        else:
            yield arg


def flatten(*iterables):
    return list(iflatten(*iterables))


def iflatten_values(*args):
    for arg in args:
        if is_collection(arg):
            if isinstance(arg, dict):
                arg = arg.values()
            for item in iflatten_values(*arg):
                yield item
        else:
            yield arg


def flatten_values(*iterables):
    return list(iflatten_values(*iterables))
