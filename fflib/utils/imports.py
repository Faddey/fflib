import importlib


__all__ = ['get_import_path', 'load_by_import_path']


def get_import_path(obj):
    return '{}:{}'.format(obj.__module__, obj.__qualname__)


def load_by_import_path(import_path):
    module_name, attr_path = import_path.split(':')
    obj = importlib.import_module(module_name)
    for attr in attr_path.split('.'):
        obj = getattr(obj, attr)
    return obj

