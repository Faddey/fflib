
__all__ = ['empty', 'hasitem', 'abbr_matches', 'UndefinedAttr',
           'is_collection', 'is_none', 'is_not_none', 'get_nested_attr']

empty = object()


def hasitem(obj, item):
    try:
        obj[item]
        return True
    except KeyError:
        return False


def abbr_matches(abbreviation, name):
    if abbreviation in name:
        return True

    def _matches_inner(abbr, tokens):
        if not tokens:
            return not bool(abbr)
        if not abbr:
            return True
        for i in range(len(abbr)+1):
            if tokens[0].startswith(abbr[:i]):
                if _matches_inner(abbr[i:], tokens[1:]):
                    return True
        return False

    all_tokens = list(filter(bool, name.lower().split('_')))
    return _matches_inner(abbreviation.lower(), all_tokens)


class UndefinedAttr:
    def __get__(self, instance, owner):
        raise NotImplementedError("Attribute not defined")


def is_collection(obj):
    if isinstance(obj, str):
        return False
    try:
        iter(obj)
    except (TypeError, ValueError):
        return False
    else:
        return True


def is_none(obj):
    return obj is None


def is_not_none(obj):
    return obj is not None


def get_nested_attr(obj, attr_path, skip=0):
    if isinstance(attr_path, str):
        attr_path = attr_path.split('.')
    for attr in attr_path[skip:]:
        obj = getattr(obj, attr)
    return obj
