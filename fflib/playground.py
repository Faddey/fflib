import os
import sys
import code
import glob
import site
import six
from functools import partial

from .utils import abbr_matches


def list_playground_modules(playground_root):
    files = glob.glob(os.path.join(playground_root, "[a-zA-Z]*.py"))
    return [
        os.path.split(p)[1].rpartition('.')[0]
        for p in files
    ]


def detect_modules(abbreviations, modules):
    not_found = []
    multiple_variants = {}
    result = []
    for abbr in abbreviations:
        matched = list(filter(partial(abbr_matches, abbr), modules))
        if not matched:
            not_found.append(abbr)
            continue
        if len(matched) > 1:
            startmatched = [m for m in matched if m.startswith(abbr)]
            if len(startmatched) == 1:
                matched = [startmatched[0]]
            else:
                multiple_variants[abbr] = matched
                continue
        result.append(matched[0])

    if not_found:
        print('Not found modules for input: "{}"'.format('", "'.join(not_found)))
    if multiple_variants:
        print('Modules specified ambiguously:')
        for abbr, variants in multiple_variants.items():
            print('    "{}": {}'.format(abbr, ', '.join(variants)))
    if not_found or multiple_variants:
        exit(1)
    return result


def load_settings():
    rc_path = os.getenv('PLAYGROUND_RC',
                        os.path.join(os.getcwd(), '.playground-rc.py'))
    if os.path.isfile(rc_path):
        settings = {'__file__': rc_path}
        with open(rc_path) as f:
            six.exec_(f.read(), settings)
    else:
        settings = {}

    settings.setdefault('ROOT', 
                        os.getenv('PLAYGROUND_ROOT', 
                                  os.path.join(os.getcwd(), 'playground')))
    settings.setdefault('MODULE', os.getenv('PLAYGROUND_MODULE', 'playground'))

    return validate_settings(settings)


def validate_settings(settings):

    root = str(settings.get('ROOT'))
    if not os.path.isdir(root):
        raise AssertionError("Playground root is incorrect: " + root)

    module = str(settings.get('MODULE'))
    try:
        __import__(module)
    except ImportError:
        raise AssertionError("Playground module is not importable: " + module)

    return {
        'ROOT': root,
        'MODULE': module
    }


class Playground:

    def __init__(self, playground_package, modules):
        self.package = playground_package
        self.module_list = modules
        self.namespace = {}

    def setup(self, module_path='__playground__'):
        sys.modules[module_path] = self

    def load(self, clear=False):
        if clear:
            self.namespace.clear()
        self.namespace['__name__'] = '__playground__'
        codelines = '\n'.join(
            'from {}.{} import *'.format(self.package, mod)
            for mod in self.module_list
        )
        six.exec_(codelines, self.namespace)


def main():
    site.addsitedir(os.getcwd())
    settings = load_settings()
    playground_root = settings['ROOT']
    playground_module = settings['MODULE']

    argv = sys.argv[1:]

    interact = '-' not in argv
    if argv and (argv[-1] == '-'):
        argv.pop()

    modules = detect_modules(argv, list_playground_modules(playground_root))

    playground = Playground(playground_module, modules)
    playground.setup()
    playground.load()

    if interact:
        code.interact(local=playground.namespace)
