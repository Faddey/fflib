
class StaticRouter(object):

    def __init__(self, route_holder=None):
        self._routes = []
        if route_holder is None:
            route_holder = self
        for attr in dir(route_holder):
            method = getattr(route_holder, attr)
            if not callable(method) or not hasattr(method, '__route_options__'):
                continue
            args, kwargs = method.__route_options__
            acceptor = self.build_accept_function(*args, **kwargs)
            self._routes.append((acceptor, method))

    def build_accept_function(self, *args, **kwargs):
        raise NotImplementedError

    def select_route(self, datum):
        for accept, handler in self._routes:
            if accept(datum):
                return handler
        if hasattr(self, 'default_route'):
            return self.default_route
        else:
            raise NotImplementedError(
                '{}: no handlers defined for {}'
                .format(type(self).__name__, repr(datum))
            )


def route(*args, **kwargs):
    def decorator(function):
        function.__route_options__ = (args, kwargs)
        return function
    return decorator


class TypeBasedRouter(StaticRouter):

    def build_accept_function(self, *types):
        return lambda datum: isinstance(datum, types)
