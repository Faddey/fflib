from fflib.utils import hasitem


__all__ = ['Stats', 'SUM', 'COUNT', 'AVG']


class Stats(object):

    def __init__(self, *col_names):
        self._col_name_map = dict(enumerate(col_names))
        self._table = []

    def add(self, *values, **kwvalues):
        kwvalues.update({
            self._get_col(idx): val
            for idx, val in enumerate(values)
        })
        self._table.append(kwvalues)

    def filter(self, column, predicate, skip_nones=True):
        col = self._get_col(column)
        filtered_table = [
            entry for entry in self._table
            if (not hasitem(entry, col) and not skip_nones or
                hasitem(entry, col) and predicate(entry[col]))
        ]
        return self._produce(filtered_table)

    def groupby(self, *columns, **aggregates):
        cols = list(map(self._get_col, columns))
        table = [entry for entry in self._table
                 if all(hasitem(entry, col) for col in cols)]
        grouped = {}
        for entry in table:
            key = frozenset((col, entry[col]) for col in cols)
            if key not in grouped:
                grouped[key] = {
                    agg_col: agg_proto.copy()
                    for agg_col, agg_proto in aggregates.items()
                }
            for agg in grouped[key].values():
                agg += entry

        ret = Stats()
        ret._col_name_map.update(self._col_name_map)
        for key, agg_dict in grouped.items():
            ret._table.append(dict(key, **{
                col: agg.result()
                for col, agg in agg_dict.items()
            }))
        return ret

    def count(self):
        return len(self._table)

    def _produce(self, table):
        ret = Stats()
        ret._col_name_map = self._col_name_map.copy()
        ret._table = table
        return ret

    def _get_col(self, column):
        return self._col_name_map.get(column, column)


class Aggregate(object):

    def __new__(cls, *args, **kwargs):
        obj = super(Aggregate, cls).__new__(cls, *args, **kwargs)
        obj._args = args
        obj._kwargs = kwargs
        return obj

    def copy(self):
        return type(self)(*self._args, **self._kwargs)

    def __init__(self, column):
        self.column = column

    def __iadd__(self, entry):
        if self.column == '*':
            self.add(entry)
        if hasitem(entry, self.column):
            self.add(entry[self.column])

    def add(self, val):
        raise NotImplementedError

    def result(self):
        raise NotImplementedError


class SUM(Aggregate):

    def __init__(self, column):
        super(SUM, self).__init__(column)
        self._sum = 0

    def add(self, val):
        self._sum += val

    def result(self):
        return self._sum


class COUNT(Aggregate):

    def __init__(self, column):
        super(COUNT, self).__init__(column)
        self._cnt = 0

    def add(self, val):
        self._cnt += 1

    def result(self):
        return self._cnt


class AVG(Aggregate):

    def __init__(self, column):
        super(AVG, self).__init__(column)
        self._cnt = 0
        self._sum = 0

    def add(self, val):
        self._cnt += 1
        self._sum += val

    def result(self):
        return self._sum / self._cnt
