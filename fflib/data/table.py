# from fflib.utils.lock import Lock
try:
    from itertools import izip
except ImportError:
    izip = zip


class Table(object):

    _rownames = None
    _colnames = None

    _rownames_set = None
    _colnames_set = None

    _values = None

    _roworder = None
    _colorder = None

    _row_reorder_lock = None
    _col_reorder_lock = None

    # #### CONSTRUCTORS

    def __init__(self):
        self._rownames = []
        self._colnames = []
        self._values = {}

        self._rownames_set = set()
        self._colnames_set = set()

        self._roworder = []
        self._colorder = []

        self._col_reorder_lock = Lock()
        self._row_reorder_lock = Lock()

    def fromfunction(self, function):
        other = Table()

        other._rownames.extend(self._rownames)
        other._colnames.extend(self._colnames)
        other._values = {key: function(key, value) for key, value in self._values.iteritems()}

        other._rownames_set.update(self._rownames)
        other._colnames_set.update(self._colnames)

        other._roworder.extend(self._roworder)
        other._colorder.extend(self._colorder)

        return other

    # #### ACCESS

    def __getitem__(self, key):
        assert isinstance(key, tuple), 'Key must be tuple (colname, rowname), expected {}'.format(type(key).__name__)
        assert len(key) == 2, 'Key must be tuple of 2 elements: (colname, rowname), expected {}'.format(len(key))
        assert key[0] in self._colnames_set, 'Invalid column name: {}'.format(repr(key[0]))
        assert key[1] in self._rownames_set, 'Invalid row name: {}'.format(repr(key[1]))
        return self._values[key]

    def columns(self):
        return list(self.itercolnames())

    def rows(self):
        return list(self.iterrownames())

    def getrowname(self, index):
        return self._rownames[self._roworder[index]]

    def getcolname(self, index):
        return self._colnames[self._colorder[index]]

    def getrow(self, name):
        return [(colname, self._values[colname, name]) for colname in self.itercolnames()]

    def getcol(self, name):
        return [(rowname, self._values[name, rowname]) for rowname in self.iterrownames()]

    def getrowdict(self, name):
        return {colname: self._values[name, colname] for colname in self.itercolnames()}

    def getcoldict(self, name):
        return {rowname: self._values[name, rowname] for rowname in self.iterrownames()}

    # #### MODIFICATIONS

    def __setitem__(self, key, value):
        assert isinstance(key, tuple), 'Key must be tuple (colname, rowname), expected {}'.format(type(key).__name__)
        assert len(key) == 2, 'Key must be tuple of 2 elements: (colname, rowname), expected {}'.format(len(key))
        assert key[0] in self._colnames_set, 'Invalid column name: {}'.format(repr(key[0]))
        assert key[1] in self._rownames_set, 'Invalid row name: {}'.format(repr(key[1]))
        self._values[key] = value

    def append_row(self, name, values=None):
        assert name not in self._rownames_set
        self._row_reorder_lock.raise_for_free()
        if values is not None:
            assert len(self._colnames) == len(values)
            for colname, value in izip(self.itercolnames(), values):
                self._values[colname, name] = value
        else:
            for colname in self.itercolnames():
                self._values[colname, name] = None
        self._roworder.append(len(self._rownames))
        self._rownames.append(name)
        self._rownames_set.add(name)

    def append_col(self, name, values=None):
        assert name not in self._colnames_set
        self._col_reorder_lock.raise_for_free()
        if values is not None:
            assert len(self._rownames) == len(values)
            for rowname, value in izip(self.iterrownames(), values):
                self._values[name, rowname] = value
        else:
            for rowname in self.iterrownames():
                self._values[name, rowname] = None
        self._colorder.append(len(self._colnames))
        self._colnames.append(name)
        self._colnames_set.add(name)

    def rename_row(self, old_name, new_name):
        assert old_name in self._rownames_set
        if old_name == new_name:
            return
        assert new_name not in self._rownames_set

        for colname in self.itercolnames():
            self._values[colname, new_name] = self._values[colname, old_name]
            del self._values[colname, old_name]

        index = self._rownames.index(old_name)
        self._rownames[index] = new_name

        self._rownames_set.remove(old_name)
        self._rownames_set.add(new_name)

    def rename_col(self, old_name, new_name):
        assert old_name in self._colnames_set
        if old_name == new_name:
            return
        assert new_name not in self._colnames_set

        for rowname in self.iterrownames():
            self._values[new_name, rowname] = self._values[old_name, rowname]
            del self._values[old_name, rowname]

        index = self._colnames.index(old_name)
        self._colnames[index] = new_name

        self._colnames_set.remove(old_name)
        self._colnames_set.add(new_name)

    # #### REORDERING

    def reorder_cols(self, order, as_patch=True):
        self._col_reorder_lock.raise_for_free()
        assert len(order) == len(self._colorder)
        if as_patch:
            self._colorder = [self._colorder[index] for index in order]
        else:
            self._colorder = list(order)

    def reorder_rows(self, order, as_patch=True):
        self._row_reorder_lock.raise_for_free()
        assert len(order) == len(self._roworder)
        if as_patch:
            self._roworder = [self._roworder[index] for index in order]
        else:
            self._roworder = list(order)

    def sort_by_col(self, name, key=None, cmp=None, reverse=False):
        order, sorted_values = self._sort_values(self.itercol(name), key, cmp, reverse)
        self.reorder_rows(order)

    def sort_by_row(self, name, key=None, cmp=None, reverse=False):
        order, sorted_values = self._sort_values(self.iterrow(name), key, cmp, reverse)
        self.reorder_cols(order)

    def sort_col(self, name, key=None, cmp=None, reverse=False):
        order, sorted_values = self._sort_values(self.itercol(name), key, cmp, reverse)
        rownames = list(self.iterrownames())
        for index, value in enumerate(sorted_values):
            self._values[name, rownames[index]] = value

    def sort_row(self, name, key=None, cmp=None, reverse=False):
        order, sorted_values = self._sort_values(self.iterrow(name), key, cmp, reverse)
        colnames = list(self.itercolnames())
        for new_index, old_index in enumerate(order):
            self._values[colnames[new_index], name] = sorted_values[old_index]

    @staticmethod
    def _sort_values(values, key, cmp, reverse):
        enumerated = list(enumerate(values))
        if cmp is not None:
            _cmp = cmp
            cmp = lambda item: _cmp(item[1])
        if key is not None:
            _key = key
            key = lambda item: _key(item[1])
        else:
            key = lambda item: item[1]
        enumerated.sort(
            cmp=cmp,
            key=key,
            reverse=reverse
        )
        return zip(*enumerated)

    # #### ITERATIONS

    def iterrownames(self):
        self._row_reorder_lock.acquire()
        for index in self._roworder:
            yield self._rownames[index]
        self._row_reorder_lock.release()

    def itercolnames(self):
        self._col_reorder_lock.acquire()
        for index in self._colorder:
            yield self._colnames[index]
        self._col_reorder_lock.release()

    def __iter__(self):
        for colname in self.itercolnames():
            for rowname in self.iterrownames():
                yield colname, rowname

    def iteritems(self):
        for colname in self.itercolnames():
            for rowname in self.iterrownames():
                yield (colname, rowname), self._values[colname, rowname]

    def iterrow(self, name):
        assert name in self._rownames_set
        for colname in self.itercolnames():
            yield self._values[colname, name]

    def enumrow(self, name):
        return izip(self.itercolnames(), self.iterrow(name))

    def itercol(self, name):
        assert name in self._colnames_set
        for rowname in self.iterrownames():
            yield self._values[name, rowname]

    def enumcol(self, name):
        return izip(self.iterrownames(), self.itercol(name))

    # #### OTHER

    def __str__(self):
        from StringIO import StringIO
        out = StringIO()

        strlen = lambda x: len(str(x))
        def format_value(value, printlen, offset=1):
            value = list(str(value))
            result = [' ']*printlen
            result[offset: offset + len(value)] = value
            return ''.join(result)

        max_row_name_len = max(map(strlen, self.iterrownames()))

        colspaces = [max(map(strlen, self.itercol(colname))) for colname in self.itercolnames()]
        colspaces = map(
            lambda item: colspaces[item[0]] if colspaces[item[0]] > strlen(item[1]) else strlen(item[1]),
            enumerate(self.itercolnames())
        )

        print >> out, format_value(' ', max_row_name_len+2), '|',
        for colname, colspace in izip(self.itercolnames(), colspaces):
            print >> out, format_value(colname, colspace+2),
        print >> out, '\n', '='*(max_row_name_len+2 + 1 + len(self._colnames)*2 + sum(colspaces) + len(colspaces) + 1),
        for rowname in self.iterrownames():
            print >> out, '\n', format_value(rowname, max_row_name_len+2), '|',
            for value, colspace in izip(self.iterrow(rowname), colspaces):
                print >> out, format_value(value, colspace+2),

        result = out.getvalue()
        out.close()
        return result

__all__ = ['Table']
