

class TreeIterator(object):

    def __init__(self, root_node, yield_inner=True):
        self._position_stack = []
        self._root_node = root_node
        self._yield_inner = yield_inner

    def _push_stack(self, node):
        frame = self._StackFrame(node, self.get_children(node))
        self._position_stack.append(frame)
        return frame

    def _next_node(self):
        if not self._position_stack:
            return self._push_stack(self._root_node).node
        while True:
            node = self._position_stack[-1].step_next()
            if node is not None:
                if not self.is_leaf(node):
                    self._push_stack(node)
                    if self._yield_inner:
                        return node
                else:
                    return node
            else:
                self._position_stack.pop()
                if len(self._position_stack) == 0:
                    raise StopIteration

    def next(self):
        return self.get_value(self._next_node())
    __next__ = next

    def __iter__(self):
        return self

    def get_value(self, node):
        raise NotImplementedError

    def get_children(self, node):
        raise NotImplementedError

    def is_leaf(self, node):
        raise NotImplementedError

    class _StackFrame(object):

        def __init__(self, node, children, next_idx=0):
            self.node = node
            self.children = children
            self.next_idx = next_idx

        def step_next(self):
            try:
                node = self.children[self.next_idx]
            except IndexError:
                return None
            else:
                self.next_idx += 1
                return node

    @property
    def path(self):
        get_value = self.get_value
        return [get_value(frame.node) for frame in self._position_stack]
