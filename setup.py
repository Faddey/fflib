from distutils.core import setup


setup(
    name='fflib', version='1.1.1',
    description='Common snippets and utilities library',
    author='Yaroslav Protsenko', author_email='yarik.pro.ph@gmail.com',
    url='https://bitbucket.org/Faddey/fflib/overview',
    packages=['fflib', 'fflib.data', 'fflib.utils'],
    scripts=['fflib/bin/play.py'],
    entry_points={'console_scripts': [
        'play = fflib.playground:main',
    ]},
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries',
    ],
    install_requires=['six'],
)
